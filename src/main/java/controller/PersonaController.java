package controller;


import dto.PersonaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {
/*@Autowired
        private PersonaDto personaDto;*/
       @RequestMapping(value = "/persona")
    public PersonaDto objPersona(){
        PersonaDto objpersona = new PersonaDto();
        objpersona.setNombre("Albert");
        objpersona.setApellido("Rodriguez");
        return objpersona;
    }

}
